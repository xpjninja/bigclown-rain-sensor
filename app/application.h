#ifndef _APPLICATION_H
#define _APPLICATION_H

#ifndef VERSION
#define VERSION "vdev"
#endif

#include <bcl.h>

#define AUTOMATIC_RESTART_INTERVAL_MS (60 * 60 * 1000) // 1 hour
#define BATTERY_UPDATE_INTERVAL_MS   (5 * 60 * 1000) // 5 min
#define TEMPERATURE_PUB_NO_CHANGE_INTERVAL_MS (5 * 60 * 1000) // 5 min
#define TEMPERATURE_PUB_VALUE_CHANGE 0.2f

#define TEMPERATURE_UPDATE_INTERVAL_MS (1 * 60 * 1000) // 5 min

#define RAIN_SENSOR_UPDATE_INTERVAL_MS   (1 * 60 * 1000) // 5 min

#define RAIN_GAUGE_ONE_TICK_MM 0.3

typedef struct
{
    uint8_t channel;
    float value;
    bc_tick_t next_pub;

} event_param_t;

#endif // _APPLICATION_H
