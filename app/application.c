#include <application.h>

bc_led_t led;

bc_tmp112_t tmp112;
static event_param_t temperature_event_param = { .next_pub = 0 };

bc_switch_t rain_gauge;

int rain_total_ticks = 0;
int rain_ticks = 0;

void restart()
{
    bc_log_info("reset required");
    bc_system_reset();
}

void battery_event_handler(bc_module_battery_event_t event, void *event_param)
{
    (void) event_param;

    float voltage;

    if (event == BC_MODULE_BATTERY_EVENT_UPDATE)
    {
        if (bc_module_battery_get_voltage(&voltage))
        {
            if (!bc_radio_pub_battery(&voltage)) {
                restart();
            }
        }
    }
}

void tmp112_event_handler(bc_tmp112_t *self, bc_tmp112_event_t event, void *event_param)
{
    float value;
    event_param_t *param = (event_param_t *)event_param;

    if (event == BC_TMP112_EVENT_UPDATE)
    {
        if (bc_tmp112_get_temperature_celsius(self, &value))
        {
            if ((fabsf(value - param->value) >= TEMPERATURE_PUB_VALUE_CHANGE) || (param->next_pub < bc_scheduler_get_spin_tick()))
            {
                bc_radio_pub_temperature(param->channel, &value);

                param->value = value;
                param->next_pub = bc_scheduler_get_spin_tick() + TEMPERATURE_PUB_NO_CHANGE_INTERVAL_MS;
            }
        }
    }
}

void rain_sensor_handler(bc_switch_t *self, bc_switch_event_t event, void *event_param)
{
    (void) self;
    (void) event_param;

    if (event == BC_SWITCH_EVENT_OPENED)
    {
        rain_total_ticks++;
        rain_ticks++;

        bc_log_debug("Rain sensor tick. Current %d. Total %d", rain_ticks, rain_total_ticks);
    }
}

void send_rain_data(void *param)
{
    (void) param;

    bc_log_info("Sending rain sensor values");

    float rain_mm = rain_ticks * RAIN_GAUGE_ONE_TICK_MM;
    float rain_total_mm = rain_total_ticks  * RAIN_GAUGE_ONE_TICK_MM;

    bc_radio_pub_int("rain-sensor/a/rain-ticks", &rain_ticks);
    bc_radio_pub_int("rain-sensor/a/rain-total-ticks", &rain_total_ticks);
    bc_radio_pub_float("rain-sensor/a/rain-mm",&rain_mm);
    bc_radio_pub_float("rain-sensor/a/rain-total-mm",&rain_total_mm);

    rain_ticks = 0;

    bc_scheduler_plan_current_relative(RAIN_SENSOR_UPDATE_INTERVAL_MS);
}

void send_and_restart()
{
    send_rain_data(NULL);
    restart();
}

void __unused application_init(void)
{
    bc_log_init(BC_LOG_LEVEL_DUMP, BC_LOG_TIMESTAMP_ABS);
    bc_log_info("Initializing Rain sensor module");

    bc_led_init(&led, BC_GPIO_LED, false, false);
    bc_led_set_mode(&led, BC_LED_MODE_OFF);

    bc_radio_init(BC_RADIO_MODE_NODE_SLEEPING);
    bc_radio_pairing_request("rain-sensor", VERSION);

    bc_module_battery_init();
    bc_module_battery_set_event_handler(battery_event_handler, NULL);
    bc_module_battery_set_update_interval(BATTERY_UPDATE_INTERVAL_MS);

    temperature_event_param.channel = BC_RADIO_PUB_CHANNEL_R1_I2C0_ADDRESS_ALTERNATE;
    bc_tmp112_init(&tmp112, BC_I2C_I2C0, 0x49);
    bc_tmp112_set_event_handler(&tmp112, tmp112_event_handler, &temperature_event_param);
    bc_tmp112_set_update_interval(&tmp112, TEMPERATURE_UPDATE_INTERVAL_MS);

    bc_scheduler_register(send_and_restart, NULL, AUTOMATIC_RESTART_INTERVAL_MS);

    bc_switch_init(&rain_gauge, BC_GPIO_P4, BC_SWITCH_TYPE_NC, BC_SWITCH_PULL_UP_DYNAMIC);
    bc_switch_set_event_handler(&rain_gauge, rain_sensor_handler, NULL);

    bc_scheduler_task_id_t send_rain_data_task = bc_scheduler_register(send_rain_data, NULL, 0);
    bc_scheduler_plan_relative(send_rain_data_task, RAIN_SENSOR_UPDATE_INTERVAL_MS);

    bc_led_pulse(&led, 2000);
    bc_log_info("Rain sensor module initialized");
}
