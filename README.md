# bigclown-rain-sensor

BigClown Rain Sensor based on Core and Sensor module with tipping bucket rain gauge

* Tipping bucket rain gauge
* https://developers.bigclown.com/hardware/about-sensor-module#buttons-magnetic-switches-phototransistors
* https://forum.bigclown.com/t/wind-and-rain-detector/355

## Tipping Bucket Rain Gauge Calibration

* s = area of gauge (`s = 100cm2` in my case)
* fill syringe with eg. 20ml of water
* press syringe slowly and drip water into gauge
* count bucket tips
* after few tips (`n = 4`) you can get exact number of (`x = 12`) ml dripped into gauge

`x1 = x / n  = 3ml = 3 cm3`

`h = x1 / s = 3 / 100 = 0,03cm = 0,3mm`

Use this value in `application.h`

`#define RAIN_GAUGE_ONE_TICK_MM 0.3`

## BigClown bridge to Influx Configuration

https://developers.bigclown.com/integrations/grafana-for-visualization#connect-mosquitto-and-influxdb

Part of the configuration file (`/etc/bigclown/mqtt2influxdb.yml`)

```
  - measurement: rain-ticks
    topic: node/+/rain-sensor/+/rain-ticks
    fields:
      value: $.payload
    tags:
      id: $.topic[1]
      channel: $.topic[3]

  - measurement: rain-ticks-total
    topic: node/+/rain-sensor/+/rain-ticks-total
    fields:
      value: $.payload
    tags:
      id: $.topic[1]
      channel: $.topic[3]

  - measurement: rain-mm
    topic: node/+/rain-sensor/+/rain-mm
    fields:
      value: $.payload
    tags:
      id: $.topic[1]
      channel: $.topic[3]

  - measurement: rain-total-mm
    topic: node/+/rain-sensor/+/rain-total-mm
    fields:
      value: $.payload
    tags:
      id: $.topic[1]
      channel: $.topic[3]
```